﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio07
{
    public partial class Form1 : Form
    {
        string[] fondos = new[]{ "Arial","CASTELLAR","Marlett" };
        public Form1()
        {
            InitializeComponent();
            CargarFondos();
        }

        private void CargarFondos() {
            for (int i = 0; i < fondos.Length; i++)
            {
                cbFuente.Items.Add(fondos[i]);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtControl.Visible = txtControl.Visible == true ? false : true;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            txtControl.Visible = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtControl.Text = txtEnvio.Text.Trim();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            txtControl.Enabled = txtControl.Enabled == true ? false : true;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Random ram = new Random();
            int red = ram.Next(1, 255);
            int gren = ram.Next(1, 255);
            int blue = ram.Next(1, 255);
            txtControl.ForeColor= Color.FromArgb(red,gren,blue)  ;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Random ram = new Random();
            int red = ram.Next(1, 255);
            int gren = ram.Next(1, 255);
            int blue = ram.Next(1, 255);
            txtControl.BackColor = Color.FromArgb(red, gren, blue);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            txtControl.Clear();
        }

        private void cbFuente_MouseCaptureChanged(object sender, EventArgs e)
        {
            int index = cbFuente.SelectedIndex;
            if (index >= 0)
            {
                string fuente = fondos[index];
                txtControl.Font = new Font(fuente, 16);
            }
        }
    }
}
