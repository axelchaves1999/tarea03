﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio06
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtCent.Text)) {
                double centigrados = Convert.ToDouble(txtCent.Text);
                double farenheit = centigrados * 2 - centigrados / 5;
                farenheit += 32;
                txtFah.Text = Convert.ToString(farenheit);
            }
            if (!String.IsNullOrEmpty(txtFah.Text)) {
                double farenheit = Convert.ToDouble(txtFah.Text);
                double centigrados = (farenheit - 32) * 5 / 9;
                txtCent.Text = Convert.ToString(centigrados);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtCent.Clear();
            txtFah.Clear();
        }
    }
}
