﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio05
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
         
        private void button1_Click(object sender, EventArgs e)
        {
            int limite = Int32.Parse(txtNumPrimo.Text);
            int divisores = 0;
            for (int i = 1; i < limite; i++)
            {
                if (EsPrimo(i))
                {
                    listNumeros.Items.Add(String.Format("El numero: {0} es primo", i));
                }

            }
        }
        private bool EsPrimo(int numero)
        {
            int divisores = 0;
            for (int i = 1; i < 100; i++)
            {
                if (numero % i == 0)
                {
                    divisores++;
                }
            }
            return divisores == 2;

        }

        private void button2_Click(object sender, EventArgs e)
        {

            int[] numeros = new int[20];
            Random ran = new Random();
            for (int i = 0; i < numeros.Length; i++)
            {
                numeros[i] = ran.Next(1, 100);
            }
            int contador = 0;
            for (int i = 0; i < numeros.Length; i++)
            {
                if (i % 2 == 0)
                {
                    listPares.Items.Add(String.Format("{0}. El numero es: {1}",contador++,numeros[i]));
                }
                else
                {
                    listImpares.Items.Add(String.Format("{0}. El numero es: {1}", contador++, numeros[i]));

                }
            }



        }
    }

}
