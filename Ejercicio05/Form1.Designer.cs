﻿namespace Ejercicio05
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.listNumeros = new System.Windows.Forms.ListBox();
            this.txtNumPrimo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.listPares = new System.Windows.Forms.ListBox();
            this.listImpares = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(251, 450);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(165, 36);
            this.button1.TabIndex = 23;
            this.button1.Text = "Iniciar programa";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // listNumeros
            // 
            this.listNumeros.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listNumeros.FormattingEnabled = true;
            this.listNumeros.ItemHeight = 23;
            this.listNumeros.Location = new System.Drawing.Point(28, 164);
            this.listNumeros.Name = "listNumeros";
            this.listNumeros.Size = new System.Drawing.Size(388, 280);
            this.listNumeros.TabIndex = 22;
            // 
            // txtNumPrimo
            // 
            this.txtNumPrimo.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumPrimo.Location = new System.Drawing.Point(28, 115);
            this.txtNumPrimo.Name = "txtNumPrimo";
            this.txtNumPrimo.Size = new System.Drawing.Size(192, 29);
            this.txtNumPrimo.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(25, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(312, 54);
            this.label1.TabIndex = 25;
            this.label1.Text = "Calcular los numeros primos desde 1 hasta:\r\n\r\n\r\n";
            // 
            // listPares
            // 
            this.listPares.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listPares.FormattingEnabled = true;
            this.listPares.ItemHeight = 23;
            this.listPares.Location = new System.Drawing.Point(515, 164);
            this.listPares.Name = "listPares";
            this.listPares.Size = new System.Drawing.Size(305, 280);
            this.listPares.TabIndex = 26;
            // 
            // listImpares
            // 
            this.listImpares.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listImpares.FormattingEnabled = true;
            this.listImpares.ItemHeight = 23;
            this.listImpares.Location = new System.Drawing.Point(848, 164);
            this.listImpares.Name = "listImpares";
            this.listImpares.Size = new System.Drawing.Size(305, 280);
            this.listImpares.TabIndex = 27;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(512, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(643, 90);
            this.label2.TabIndex = 28;
            this.label2.Text = "El siguiente programa registra los numero que se encuentrar en posiciones pares y" +
    " impares\r\n\r\n\r\n\r\n\r\n";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(756, 463);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(165, 36);
            this.button2.TabIndex = 29;
            this.button2.Text = "Iniciar programa";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1165, 560);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.listImpares);
            this.Controls.Add(this.listPares);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.listNumeros);
            this.Controls.Add(this.txtNumPrimo);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Multiples ejercicios";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListBox listNumeros;
        private System.Windows.Forms.TextBox txtNumPrimo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listPares;
        private System.Windows.Forms.ListBox listImpares;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button2;
    }
}

