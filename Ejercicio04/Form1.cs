﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio04
{
    public partial class Suma : Form
    {
        public Suma()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listNumeros.Items.Clear();
            if (cbResultados.Checked == true) {
                int limite = Int32.Parse(txtNum.Text);
                int resultado = 0;
                for (int i = 1; i < limite; i++)
                {
                    resultado += i;
                    listNumeros.Items.Add(String.Format("{0}. suma parcial: {1}", i,resultado));

                }
                listNumeros.Items.Add(String.Format("El resultado es: {0}",resultado));
            }
        }
    }
}
