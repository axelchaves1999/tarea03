﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tarea03
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int num1 = LeerInt(txtNum1.Text.Trim());
            int num2 = LeerInt(txtNum2.Text.Trim());
            txtResultado.Text = Convert.ToString(num2 + num1);
            Limpiar();
        }
        private void Limpiar() {
            txtNum1.Clear();
            txtNum2.Clear();
            txtResultado.Clear();
        }

        private int LeerInt(string text)
        {

            int num = 0;
            if (!Int32.TryParse(text, out num))
            {
                MessageBox.Show("Debe ingresar numeros");
            }
            else
            {
                return num;
            }
            return 0;


        }
    }
}
