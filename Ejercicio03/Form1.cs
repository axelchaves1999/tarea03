﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio03
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int total = 0;
            if (rbNomral.Checked == true)
            {
                total += 100;
            }
            else if (rbPersonal.Checked == true)
            {
                total += 50;
            }
            else {
                total += 150;
            }
            if (cbEmail.Checked == true) {
                total += 5;
            }
            if (cbTelefono.Checked == true) {
                total += 15;
            }
            if (cbFax.Checked == true) {
                total += 25;
            }
            MessageBox.Show("El total de gastos del envio es: "+total.ToString("c1"));

        }
    }
}
