﻿namespace Ejercicio03
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.rbPersonal = new System.Windows.Forms.RadioButton();
            this.rbUrgente = new System.Windows.Forms.RadioButton();
            this.rbNomral = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.cbEmail = new System.Windows.Forms.CheckBox();
            this.cbFax = new System.Windows.Forms.CheckBox();
            this.cbTelefono = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(20, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(478, 22);
            this.label1.TabIndex = 14;
            this.label1.Text = "Como desea recibir el pedido? (Marca solo una opción)";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // rbPersonal
            // 
            this.rbPersonal.AutoSize = true;
            this.rbPersonal.Checked = true;
            this.rbPersonal.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbPersonal.Location = new System.Drawing.Point(24, 94);
            this.rbPersonal.Name = "rbPersonal";
            this.rbPersonal.Size = new System.Drawing.Size(207, 27);
            this.rbPersonal.TabIndex = 18;
            this.rbPersonal.TabStop = true;
            this.rbPersonal.Text = "Por correo personal ($50)";
            this.rbPersonal.UseVisualStyleBackColor = true;
            // 
            // rbUrgente
            // 
            this.rbUrgente.AutoSize = true;
            this.rbUrgente.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbUrgente.Location = new System.Drawing.Point(24, 160);
            this.rbUrgente.Name = "rbUrgente";
            this.rbUrgente.Size = new System.Drawing.Size(237, 27);
            this.rbUrgente.TabIndex = 19;
            this.rbUrgente.TabStop = true;
            this.rbUrgente.Text = "Por paqueteria urgente ($150)";
            this.rbUrgente.UseVisualStyleBackColor = true;
            // 
            // rbNomral
            // 
            this.rbNomral.AutoSize = true;
            this.rbNomral.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbNomral.Location = new System.Drawing.Point(24, 127);
            this.rbNomral.Name = "rbNomral";
            this.rbNomral.Size = new System.Drawing.Size(232, 27);
            this.rbNomral.TabIndex = 20;
            this.rbNomral.TabStop = true;
            this.rbNomral.Text = "Por paqueteria normal ($100)";
            this.rbNomral.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 208);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(682, 22);
            this.label2.TabIndex = 21;
            this.label2.Text = "Como desea ser notificado del pedido? (Marca todas las opciones que deseas)";
            // 
            // cbEmail
            // 
            this.cbEmail.AutoSize = true;
            this.cbEmail.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbEmail.Location = new System.Drawing.Point(24, 255);
            this.cbEmail.Name = "cbEmail";
            this.cbEmail.Size = new System.Drawing.Size(126, 27);
            this.cbEmail.TabIndex = 22;
            this.cbEmail.Text = "Por email ($5)";
            this.cbEmail.UseVisualStyleBackColor = true;
            // 
            // cbFax
            // 
            this.cbFax.AutoSize = true;
            this.cbFax.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFax.Location = new System.Drawing.Point(24, 339);
            this.cbFax.Name = "cbFax";
            this.cbFax.Size = new System.Drawing.Size(118, 27);
            this.cbFax.TabIndex = 23;
            this.cbFax.Text = "Por fax ($25)";
            this.cbFax.UseVisualStyleBackColor = true;
            // 
            // cbTelefono
            // 
            this.cbTelefono.AutoSize = true;
            this.cbTelefono.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTelefono.Location = new System.Drawing.Point(24, 297);
            this.cbTelefono.Name = "cbTelefono";
            this.cbTelefono.Size = new System.Drawing.Size(152, 27);
            this.cbTelefono.TabIndex = 24;
            this.cbTelefono.Text = "Por telefono ($15)";
            this.cbTelefono.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(176, 414);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(495, 36);
            this.button1.TabIndex = 25;
            this.button1.Text = "Calcular gastos de envio";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(741, 487);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cbTelefono);
            this.Controls.Add(this.cbFax);
            this.Controls.Add(this.cbEmail);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.rbNomral);
            this.Controls.Add(this.rbUrgente);
            this.Controls.Add(this.rbPersonal);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Detalles de envio";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbPersonal;
        private System.Windows.Forms.RadioButton rbUrgente;
        private System.Windows.Forms.RadioButton rbNomral;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox cbEmail;
        private System.Windows.Forms.CheckBox cbFax;
        private System.Windows.Forms.CheckBox cbTelefono;
        private System.Windows.Forms.Button button1;
    }
}

